import { enviar } from "./Conexion";
import { save, saveToken } from "./SessionUtil";

export async function inicio_sesion(data) {

        const sesion = await enviar('examen.php', data, "");

        if (sesion.code === 200 && sesion.info && sesion.info.code) {
            saveToken(sesion.info.code);
            save('id', sesion.info.external);
            save('user', sesion.info.dni);
        }

        return sesion;
}
