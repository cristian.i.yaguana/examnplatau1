let URL = "https://computacion.unl.edu.ec/pdml/examen1/";
let URLS = "https://computacion.unl.edu.ec/pdml/examen1/examen.php/";

//devolver la url
export function url_api() {
  return URL;
}

//documentos
export async function obtenerNinosCE(recurso, key = "") {
  let headers = {};

  headers = {
    "TEST-KEY": key,
  };

  const response = await (
    await fetch(URLS + recurso, {
      method: "GET",
      headers: headers,
      cache: "no-store",
    })
  ).json();

  return response;
}



export async function enviar(recurso, data) {
  const headers = {
    Accept: "application/json",
    //"Content-Type": "application/json"
  };

  const response = await fetch(URL + recurso, {
    method: "POST",
    headers: headers,
    body: JSON.stringify(data),
  });

  return await response.json();
}
export async function obtenercurso(recurso) {
  const response = await fetch(URLS + recurso);
  return await response.json();
}
export async function enviarCenso(recurso, data, key = "") {
    let headers = {};

    if (key !== "") {
        headers = {
            'TEST-KEY': key
        };
    } else {
        headers = {

        };
    }

    const response = await fetch(URL + recurso, {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    });

    return await response.json();
}
export async function Censo(recurso, key = "") {
    let headers = {};

    headers = {
        'TEST-KEY': key
    };

    const response = await (await fetch(URL + recurso,{
        method: "GET",
        headers: headers,
        cache: 'no-store'
    })).json();

    return response;
}
export async function obtenerCensos(recurso, key = "") {
    let headers = {};

    headers = {
        'TEST-KEY': key,
    };

    const response = await (await fetch(URLS + recurso, {
        method: "GET",
        headers: headers,
        cache: 'no-store',
    })).json();
    return response;
}

export async function enviarcc(recurso, data, key = "") {
  let headers = {};

  if (key !== "") {
      headers = {
          'TEST-KEY': key
      };
  } else {
      headers = {

      };
  }

  const response = await fetch(URL + recurso, {
      method: "POST",
      headers: headers,
      body: JSON.stringify(data)
  });

  return await response.json();
}
