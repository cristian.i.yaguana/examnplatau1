'use client';
import { GuardarDocumento, enviarCenso, enviarcc, obtener, obtenerNinosCE, obtenercurso } from "@/hooks/Conexion";
import { getUser, getToken } from "@/hooks/SessionUtilClient";
import { useState } from "react";
import { useForm } from "react-hook-form";
import mensajes from '@/componentes/Mensajes';
import { useRouter } from 'next/navigation';
import Link from "next/link";


export default function Page() {
    //router
    const router = useRouter();

    let key = getToken();
    const [curso, setcurso] = useState([]);
    const [llcurso, setLlcurso] = useState(false);

    const [nino, setnino] = useState([]);
    const [llnino, setLlnino] = useState(false);

    const [cnino, setcnino] = useState([]);
    const [llcnino, setLlcnino] = useState(false);


    const [escuela, setescuela] = useState([]);
    const [llescuela, setLlescuela] = useState(false);
    const { register, handleSubmit, formState: { errors } } = useForm();
    const external = getUser();
    //const funcion = "guardarDocumento";

    

    if (!llescuela) {
        obtenerNinosCE('?resource=school', key).then((info) => {
            //console.log(info.error);
            if (info.code === 200) {
                setescuela(info.info);
                setLlescuela(true);
            }
        });
    };
    if (!llnino) {
        obtenerNinosCE('?resource=unregistered_children', key).then((info) => {
            //console.log(info.error);
            if (info.code === 200) {
                setnino(info.info);
                setLlnino(true);
            }
        });
    };

    if (!llcurso) {
        obtenercurso('?resource=course').then((info) => {
            //console.log(info.error);
            if (info.code === 200) {
                setcurso(info.info);
                setLlcurso(true);
            }
        });
    };


    const onSubmit = (data) => {

        var datos = {

            "resource": "saveCensus",
            "weight": data.weight,
            "height": data.height,
            "representative": data.representative,
            "activities": data.activities,
            "external_child": data.external_child,
            "external_school": data.external_school,
            "external_course": data.external_course,
            "external_session": external,

        };
        let estates = true;
    cnino.forEach(function (infos) {
        if (infos.external_id === data.external_nino) {
            estates = false;
        };
    })

    if (!estates) {
        mensajes("No se puede censar nuevamente a este niño", "Error", "error")
    } else {
        enviarcc('examen.php', datos, key).then((info) => {
            if (info.code === 200) {
                mensajes("Registro Exitoso", "Bien", "success")
                router.push("/censopormi");
            }
        });

    }
    if (!llcnino) {
        obtenerNinosCE('?resource=census_children', key).then((info) => {
            
            if (info.code === 200) {
                setcnino(info.info);
                setLlcnino(true);
            } else {
                mensajes("No se pudo Listar los ninos", "Error", "error");
            }
        });
    };

        
    }

    
    return (
        <div className="wrapper" >
            <center>

                <div className="d-flex flex-column" style={{ width: 700 }}>
                    <h5 className="title" style={{ color: "black", font: "bold" }}>REGISTRO DE CENSO</h5>
                    <br />
                    <div className="container">
                        <img src="https://previews.123rf.com/images/dejanj01/dejanj011508/dejanj01150800031/44249050-colecci%C3%B3n-de-iconos-que-presentan-diferentes-materias-escolares-ciencia-arte-historia-geograf%C3%ADa.jpg" className="card" style={{ width: 200, height: 125 }} />
                        <div>

                        </div>
                    </div>

                    <br />

                    <div className='container-fluid'>
                        <form className="user" onSubmit={handleSubmit(onSubmit)}>
                            <div className="row mb-4">
                                <div className="col">
                                    <input type="number" {...register('weight', { required: true })} className="form-control form-control-user" placeholder="Ingrese el peso" />
                                    {errors.weight && errors.weight.type === 'required' && <div className='alert alert-danger'>Ingrese el peso</div>}
                                </div>
                                <div className="col">
                                    <input type="number" className="form-control form-control-user" placeholder="Ingrese la altura" {...register('height', { required: true })} />
                                    {errors.height && errors.height.type === 'required' && <div className='alert alert-danger'>Ingrese la altura</div>}
                                </div>
                            </div>

                            <div className="row mb-4">
                                <div className="col">
                                    <input type="text" className="form-control form-control-user" placeholder="Ingrese el representante" {...register('representative', { required: true })} />
                                    {errors.representative && errors.representative.type === 'required' && <div className='alert alert-danger'>Ingrese el representante</div>}
                                </div>
                                <div className="col">
                                    <input type="text" className="form-control form-control-user" placeholder="Ingrese la actividad" {...register('activities', { required: true })} />
                                    {errors.activities && errors.activities.type === 'required' && <div className='alert alert-danger'>Ingrese la actividad</div>}
                                </div>
                            </div>



                            <div className="row mb-4">
                                <div className="col">
                                    <select className='form-control' {...register('external_school', { required: true })}>
                                        <option>Elija una escuela</option>
                                        {escuela.map((m, i) => {
                                            return (<option key={i} value={m.external_id}>
                                                {m.nombre}
                                            </option>)
                                        })}
                                    </select>
                                    {errors.external_school && errors.external_school.type === 'required' && <div className='alert alert-danger'>Selecione una escuela</div>}
                                </div>
                            </div>

                            <div className="row mb-4">
                                <div className="col">
                                    <select className='form-control' {...register('external_child', { required: true })}>
                                        <option>Elija un nino</option>
                                        {nino.map((n, i) => {
                                            return (<option key={i} value={n.external_id}>
                                                {n.nombres}
                                            </option>)
                                        })}
                                    </select>
                                    {errors.external_child && errors.external_child.type === 'required' && <div className='alert alert-danger'>Selecione un nino</div>}
                                </div>
                            </div>
                            <div className="row mb-4">
                                <div className="col">
                                    <select className='form-control' {...register('external_course', { required: true })}>
                                        <option>Elija el curso</option>
                                        {curso.map((c, i) => {
                                            return (<option key={i} value={c.external_id}>
                                                {c.denominacion}
                                            </option>)
                                        })}
                                    </select>
                                    {errors.external_course && errors.external_course.type === 'required' && <div className='alert alert-danger'>Selecione el curso</div>}
                                </div>
                            </div>


                            <hr />
                            <button type='submit' className="btn btn-success">GUARDAR</button>
                            <div style={{ display: 'flex', gap: '20px' }}>
                                <Link href="/curso" className="btn btn-danger btn-rounded">Cancelar</Link>
                            </div>

                        </form>
                    </div>
                </div>
            </center>
        </div>

    );

};