'use client';
import { obtenerDocumento, obtenerNinosCE } from "@/hooks/Conexion";
import { borrarSesion, getToken } from "@/hooks/SessionUtilClient";
import Link from "next/link";
import { useState } from "react";
import mensajes from "@/componentes/Mensajes";

export default function Curso() {

    let key = getToken();

    const [data, setData] = useState([]);
    const [ninoscenslist, setninoscenslist] = useState(false);

    const salir = () => {
        borrarSesion();
    }

    if (!ninoscenslist) {
        obtenerNinosCE('?resource=census_children', key).then((info) => {
            if (info.code === 200) {
                setData(info.info);
                setninoscenslist(true);
            }
        });
    }
    //console.log(data);


    return (
        <div className="row">
            <h1 style={{ textAlign: "center" }}>LISTA DE TODOS LOS CENSADOS</h1>
            <div className="container-fluid">
                <div className="col-12 mb-5" style={{ display: "flex", justifyContent: "center" }}>
                    <Link style={{ margin: "15px" }} href="/registrar" className="btn btn-warning">Registrar Nuevo Censo</Link>
                    <Link style={{ margin: "15px" }} href="/login" onClick={salir} className="btn btn-danger">Cerrar Sesión</Link>
                    <Link style={{ margin: "15px" }} href="/censopormi" className="btn btn-success">Mis registros Censo</Link>
                </div>

                <table className="table table-striped table-bordered table-responsive-sm">
                    <thead className="thead-dark">
                        <tr>
                            <th className="text-center">Nro</th>
                            <th className="text-center">NOMBRES</th>
                            <th className="text-center">EDAD</th>

                        </tr>
                    </thead>
                    <tbody>
                        {data.map((dato, i) => (
                            <tr key={i}>
                                <td className="text-center">{i + 1}</td>
                                <td>{dato.nombres}</td>
                                <td className="text-center">{dato.edad}</td>
                            </tr>
                        )
                        )}
                    </tbody>
                </table>

            </div>
        </div>
    );
}