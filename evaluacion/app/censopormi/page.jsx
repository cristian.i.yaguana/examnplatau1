'use client';
import { obtenerDocumento, obtenerNinosCE } from "@/hooks/Conexion";
import { borrarSesion, getUser, getToken } from "@/hooks/SessionUtilClient";
import Link from "next/link";
import { useState } from "react";
import mensajes from "@/componentes/Mensajes";

export default function Curso() {

    const external = getUser();

    let key = getToken();

    const [data, setData] = useState([]);
    const [ninoscenslist, setninoscenslist] = useState(false);

    const salir = () => {
        borrarSesion();
    }

    if (!ninoscenslist) {
        obtenerNinosCE('?resource=census_children_login&external='+external, key).then((info) => {
            if (info.code === 200) {
                setData(info.info);
                setninoscenslist(true);
            }
        });
    }
    //console.log(data);


    return (
        <div className="row">
            <h1 style={{ textAlign: "center" }}>MIS REGISTROS</h1>
            <div className="container-fluid">
            <div className="col-12 mb-5" style={{ display: "flex", justifyContent: "center" }}>
                    <Link style={{ margin: "15px" }} href="/registrar" className="btn btn-warning">Registrar Nuevo Censo</Link>
                    <Link style={{ margin: "15px" }} href="/login" onClick={salir} className="btn btn-danger">Cerrar Sesión</Link>
                    <Link style={{ margin: "15px" }} href="/curso" className="btn btn-success">MOSTAR TODOS LOS REGISTROS</Link>
                </div>
                <table className="table table-striped table-bordered table-responsive-sm">
                    <thead className="thead-dark">
                        <tr>
                            <th className="text-center">Nro</th>
                            <th className="text-center">NOMBRES</th>
                            <th className="text-center">EDAD</th>
                            <th className="text-center">PESO</th>
                            <th className="text-center">ALTURA</th>
                            <th className="text-center">CURSO</th>
                            <th className="text-center">REPRESENTANTE</th>
                            <th className="text-center">ACTIVIDAD</th>
                            <th className="text-center">ACCIONES</th>

                        </tr>
                    </thead>
                    <tbody>
                        {data.map((dato, i) => (
                            <tr key={i}>
                                <td className="text-center">{i + 1}</td>
                                <td>{dato.nombres}</td>
                                <td className="text-center">{dato.edad}</td>
                                <td className="text-center">{dato.peso}</td>
                                <td className="text-center">{dato.talla}</td>
                                <td className="text-center">{dato.curso}</td>
                                <td className="text-center">{dato.representante}</td>
                                <td className="text-center">{dato.actividades}</td>
                                <td>
                                    {<Link style={{ margin: "5px" }} href={`/censopormi/modificar/${dato.extrenal_censo}`} className="btn btn-primary font-weight-bold">MODIFICAR</Link>}
                            
                                </td>
                            </tr>
                        )
                        )}
                    </tbody>
                </table>

            </div>
        </div>
    );
}