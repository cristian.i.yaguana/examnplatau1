'use client';
import mensajes from "@/componentes/Mensajes";
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import Link from "next/link";
import { useRouter } from 'next/navigation';
import {  enviar, enviarCenso, obtenerCensos, obtenerNinosCE, obtenercurso } from "@/hooks/Conexion";
import { useState } from "react";
import { getUser, getToken } from "@/hooks/SessionUtilClient";
export default function Page({ params }) {

    const { external } = params;
    let key = getToken();
    const router = useRouter();
    const usuario_external = getUser();

    const [curso, setCurso] = useState([]);
    const [llcurso, setLlcurso] = useState(false);
    const [escuela, setescuela] = useState([]);
    const [llescuela, setLlescuela] = useState(false);
    const [estado, setestado] = useState(false);

    console.log("el token es "+key)

    //cagar escuelas
    if (!llescuela) {
        obtenerNinosCE('?resource=school', key).then((info) => {
            //console.log(info.error);
            if (info.code === 200) {
                setescuela(info.info);
                setLlescuela(true);
            }
        });
    };
    //cagar curso
    if (!llcurso) {
        obtenercurso('?resource=course').then((info) => {
            //console.log(info.error);
            if (info.code === 200) {
                setCurso(info.info);
                setLlcurso(true);
            }
        });
    };

    // CARGARCENSO
    if (!estado) {
        obtenerCensos('?resource=census_children_login&external=' + usuario_external, key).then((info) => {
            if (info.code === 200) {
                info.info.forEach(function (element) {
                    if (element.extrenal_censo === external) {
                        setValue('weight', element.peso);
                        setValue('height', element.talla);
                        setValue('representative', element.representante);
                        setValue('activities', element.actividades);
                        setValue('external_course', element.external_curso);
                        setValue('external_school', element.external_escuela);
                    }
                    console.log("eeeeele", element)
                })
                setestado(true);
            }
        });
    };
    console.log("estossss:  ", usuario_external)


    const validationShema = Yup.object().shape({

        weight: Yup.number().required('ingrese el peso'),
        height: Yup.number().required('Ingrese un talla/altura'),
        representative: Yup.string().required('Ingrese el representante'),
        activities: Yup.string().required('Ingrese la actividad'),

        external_school: Yup.string().required('Seleccione una escuela'),
        external_course: Yup.string().required('Seleccione un curso'),
    });

    const formOptions = { resolver: yupResolver(validationShema) };
    const { register, handleSubmit, setValue, formState } = useForm(formOptions);
    const { errors } = formState;

    const sendData = (data) => {
        var datos = {
            'weight': data.weight,
            'height': data.height,
            'representative': data.representative,
            'activities': data.activities,
            'external_course': data.external_course,
            'external_school': data.external_school,
            'external': external,
            "resource": "updateCensus"
        };

        enviarCenso('examen.php', datos, key).then((info) => {
            if (info.code === 200) {
                mensajes("Censo modificado exitosamente", "BIEN", "success")
                router.push("/censopormi");
                
            } else {
                mensajes("No se pudo modificar", "Error", "error")
                
            }
            console.log("el enviar es"+info)
        });
        

    };


    return (
        <div className="wrapper" >
            <center>

                <div className="d-flex flex-column" style={{ width: 700 }}>
                    <h5 className="title" style={{ color: "black", font: "bold" }}>MODIFICAR DE CENSO</h5>
                    <br />
                    <div className="container">
                        <img src="https://previews.123rf.com/images/dejanj01/dejanj011508/dejanj01150800031/44249050-colecci%C3%B3n-de-iconos-que-presentan-diferentes-materias-escolares-ciencia-arte-historia-geograf%C3%ADa.jpg" className="card" style={{ width: 200, height: 125 }} />
                        <div>
                        
                        </div>
                    </div>
                    
                    <br />

                    <div className='container-fluid'>
                        <form className="user" onSubmit={handleSubmit(sendData)}>
                            <div className="row mb-4">
                                <div className="col">
                                    <input type="number" {...register('weight', { required: true })} className="form-control form-control-user" placeholder="Ingrese el peso" />
                                    {errors.weight && errors.weight.type === 'required' && <div className='alert alert-danger'>Ingrese el peso</div>}
                                </div>
                                <div className="col">
                                    <input type="number" className="form-control form-control-user" placeholder="Ingrese la altura" {...register('height', { required: true })} />
                                    {errors.height && errors.height.type === 'required' && <div className='alert alert-danger'>Ingrese la altura</div>}
                                </div>
                            </div>

                            <div className="row mb-4">
                                <div className="col">
                                    <input type="text" className="form-control form-control-user" placeholder="Ingrese el representante" {...register('representative', { required: true })} />
                                    {errors.representative && errors.representative.type === 'required' && <div className='alert alert-danger'>Ingrese el representante</div>}
                                </div>
                                <div className="col">
                                    <input type="text" className="form-control form-control-user" placeholder="Ingrese la actividad" {...register('activities', { required: true })} />
                                    {errors.activities && errors.activities.type === 'required' && <div className='alert alert-danger'>Ingrese la actividad</div>}
                                </div>
                            </div>

                            

                            <div className="row mb-4">
                                <div className="col">
                                    <select className='form-control' {...register('external_school', { required: true })}>
                                        <option>Elija una escuela</option>
                                        {escuela.map((m, i) => {
                                            return (<option key={i} value={m.external_id}>
                                                {m.nombre}
                                            </option>)
                                        })}
                                    </select>
                                    {errors.external_school && errors.external_school.type === 'required' && <div className='alert alert-danger'>Selecione una escuela</div>}
                                </div>
                            </div>

                            <div className="row mb-4">
                                <div className="col">
                                    <select className='form-control' {...register('external_course', { required: true })}>
                                        <option>Elija el curso</option>
                                        {curso.map((c, i) => {
                                            return (<option key={i} value={c.external_id}>
                                                {c.denominacion}
                                            </option>)
                                        })}
                                    </select>
                                    {errors.external_course && errors.external_course.type === 'required' && <div className='alert alert-danger'>Selecione el curso</div>}
                                </div>
                            </div>

                            
                            <hr />
                            <button type='submit' className="btn btn-success">GUARDAR</button>
                            <div style={{ display: 'flex', gap: '20px' }}>
                                        <Link href="/curso" className="btn btn-danger btn-rounded">Cancelar</Link>
                                    </div>

                        </form>
                    </div>
                </div>
            </center>
        </div>
    );
}

